package com.gftbank.app.insurance.models.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gftbank.app.insurance.clientes.ISeguroClienteRest;
import com.gftbank.app.insurance.models.entity.Seguro;

@Service
public class SeguroServiceImpl implements ISeguroService {

	@Autowired
	private ISeguroClienteRest seguroFeign;
	
	@Override
	public List<Seguro> findAll() {
		return seguroFeign.listarClientes().stream().map(c -> new Seguro(c, "", new Date())).collect(Collectors.toList());
	}

	@Override
	public Seguro findById(Long id) {
		return new Seguro(seguroFeign.consultaId(id),"",new Date());
	}

}
