package com.gftbank.app.insurance.models.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Cliente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6906996860117645047L;

	private Long id;
	private String nombre;
	private String apellidos;
	private Date fechaNacimiento;
	private String sexo;

}
