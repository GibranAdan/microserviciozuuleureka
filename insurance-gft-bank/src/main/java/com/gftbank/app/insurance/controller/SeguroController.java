package com.gftbank.app.insurance.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gftbank.app.insurance.models.entity.Seguro;
import com.gftbank.app.insurance.models.service.SeguroServiceImpl;

@RestController
public class SeguroController {

	@Autowired
	private SeguroServiceImpl seguroService;
	
	@Value("${configuracion.texto}")
	private String mensaje;
	
	@GetMapping("/listar_clientes")
	public List<Seguro> listarClientes(){
		return seguroService.findAll();
	}
	
	@GetMapping("/obtener_cliente/{id}")
	public Seguro consultarSeguro(@PathVariable Long id) {
		return seguroService.findById(id);
	}
	
	@GetMapping("/obtener_config")
	public ResponseEntity<?> obtenerConfig() {
		Map<String, String> json = new HashMap<>();
		json.put(mensaje, mensaje);
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);
	}
}
