package com.gftbank.app.insurance.models.service;

import java.util.List;

import com.gftbank.app.insurance.models.entity.Seguro;

public interface ISeguroService {
	
	public List<Seguro> findAll();
	public Seguro findById(Long id);

}
