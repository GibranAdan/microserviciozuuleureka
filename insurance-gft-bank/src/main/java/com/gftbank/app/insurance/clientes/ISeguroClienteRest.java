package com.gftbank.app.insurance.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.gftbank.app.insurance.models.entity.Cliente;

@FeignClient(name = "crud-clientes", url = "localhost:8083")
public interface ISeguroClienteRest {

	@GetMapping("/listar")
	public List<Cliente> listarClientes();

	@GetMapping("/consultar/{id}")
	public Cliente consultaId(@PathVariable Long id);

	@PostMapping("/cliente/agregar")
	public Cliente agregarCliente(Cliente newCustomer);

	@DeleteMapping("cliente/borrar/{id}")
	public String borrarCliente(@PathVariable Long id);

	@PatchMapping("cliente/actualizar")
	public String actualizarCliente(Cliente newCustomer);
}
