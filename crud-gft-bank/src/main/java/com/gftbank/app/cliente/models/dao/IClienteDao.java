package com.gftbank.app.cliente.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.gftbank.app.cliente.models.entity.Cliente;

public interface IClienteDao extends CrudRepository<Cliente, Long> {

}
