package com.gftbank.app.cliente.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gftbank.app.cliente.models.entity.Cliente;
import com.gftbank.app.cliente.models.service.IClienteService;

@RestController
public class ClienteController implements IClienteController {

	@Autowired
	private IClienteService clienteService;

	@GetMapping("/listar")
	public List<Cliente> listar() {
		return clienteService.findAll();
	}

	@GetMapping("/consultar/{id}")
	public Cliente consultaId(@PathVariable Long id) {
		return clienteService.findById(id);
	}

	@PostMapping("/cliente/agregar")
	public Cliente agregarCliente(Cliente newCustomer) {
		return clienteService.saveCustomer(newCustomer);
	}

	@DeleteMapping("cliente/borrar/{id}")
	public String borrarCliente(@PathVariable Long id) {
		return clienteService.deleteCustomer(id);
	}

	@PatchMapping("cliente/actualizar")
	public String actualizarCliente(Cliente newCustomer) {
		return clienteService.updateCustomer(newCustomer);
	}

}
