package com.gftbank.app.cliente.controller;

import java.util.List;

import com.gftbank.app.cliente.models.entity.Cliente;

public interface IClienteController {

	public List<Cliente> listar();

	public Cliente consultaId(Long id);

	public Cliente agregarCliente(Cliente newCustomer);

	public String borrarCliente(Long id);

	public String actualizarCliente(Cliente newCustomer);

}
