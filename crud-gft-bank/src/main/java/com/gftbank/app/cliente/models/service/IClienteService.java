package com.gftbank.app.cliente.models.service;

import java.util.List;

import com.gftbank.app.cliente.models.entity.Cliente;

public interface IClienteService {

	public List<Cliente> findAll();

	public Cliente findById(Long id);

	public Cliente saveCustomer(Cliente newCustomer);

	public String deleteCustomer(Long id);

	public String updateCustomer(Cliente newCustomer);

}
