package com.gftbank.app.cliente.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gftbank.app.cliente.models.dao.IClienteDao;
import com.gftbank.app.cliente.models.entity.Cliente;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDao clienteDao;

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {

		return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	public Cliente saveCustomer(Cliente newCustomer) {
		return newCustomer != null ? clienteDao.save(newCustomer) : new Cliente();
	}

	@Override
	public String deleteCustomer(Long id) {
		String message = null;
		if (clienteDao.findById(id).isPresent()) {
			clienteDao.deleteById(id);
			message = "Cliente eliminado";
		} else {
			message = "El cliente no existe";
		}
		return message;
	}

	@Override
	public String updateCustomer(Cliente newCustomer) {
		Long num = newCustomer.getId();
		String message = null;
		if (clienteDao.findById(num).isPresent()) {
			Cliente customerToUpdate = new Cliente();
			customerToUpdate.setId(newCustomer.getId());
			customerToUpdate.setNombre(newCustomer.getNombre());
			customerToUpdate.setApellidos(newCustomer.getApellidos());
			customerToUpdate.setFechaNacimiento(newCustomer.getFechaNacimiento());
			customerToUpdate.setSexo(newCustomer.getSexo());
			message = "Actualización exitosa";
		} else {
			message = "Error al actualizar";
		}
		return message;
	}
}
